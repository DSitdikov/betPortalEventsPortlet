import { messagesRu } from './ru/messages.ru';
import { messagesEn } from './en/messages.en';

export const messages = {
    ru: messagesRu,
    en: messagesEn
};