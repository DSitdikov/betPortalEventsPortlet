import CommonAction from '../../../actions/common/types/common-action';
import { UiActionType } from '../../../actions/ui/ui-action-type';
import SportUiState from '../../../entities/ui/sport-ui-state/sport-ui-state';

export default function sportReducer(state: SportUiState = new SportUiState(), action: CommonAction) {
    switch (action.type) {
        case UiActionType.TOGGLE_EXPAND_STATE_OF_SPORT: {
            const resultState = new SportUiState(state);
            resultState.expanded = !state.expanded;
            return resultState;
        }
        case UiActionType.TOGGLE_EXPAND_STATE_OF_LEAGUE: {
            const resultState = new SportUiState(state);
            resultState.expanded = action.payload.sportExpanded;
            return resultState;
        }
        default:
            return state;
    }
}