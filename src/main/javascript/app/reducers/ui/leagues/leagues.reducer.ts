import CommonAction from '../../../actions/common/types/common-action';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';
import LeagueEntities from '../../../services/data-loaders/league-data-loader/league-entities';
import { UiActionType } from '../../../actions/ui/ui-action-type';
import LeagueUiState from '../../../entities/ui/league-ui-state/league-ui-state';

const initialState = new Map<number, LeagueUiState>();

export default function leaguesReducer(state: Map<number, LeagueUiState> = initialState, action: CommonAction) {

    switch (action.type) {
        case DomainModelActionType.FETCH_LEAGUES_SUCCESS: {
            const leagueEntities = action.payload as LeagueEntities;
            const leagues = leagueEntities.leagues;
            const resultState = new Map<number, LeagueUiState>(state);

            for (const leagueId of Array.from(leagues.keys())) {
                if (!resultState.has(leagueId)) {
                    resultState.set(leagueId, new LeagueUiState());
                }
            }

            return resultState;
        }
        case UiActionType.TOGGLE_SELECTION_OF_LEAGUE: {
            return toggleSelectionOfLeague(state, action.payload.leagueId);
        }
        case UiActionType.TOGGLE_SELECTION_OF_EVENT: {
            return toggleSelectionOfLeague(state, action.payload.leagueId, action.payload.leagueSelected);
        }
        case UiActionType.TOGGLE_EXPAND_STATE_OF_LEAGUE: {
            const leagueId = action.payload.leagueId as number;
            const resultState = new Map<number, LeagueUiState>(state);

            const item = new LeagueUiState(resultState.get(leagueId));
            item.expanded = !item.expanded;
            resultState.set(leagueId, item);

            return resultState;
        }
        case UiActionType.TOGGLE_EXPAND_STATE_OF_SPORT: {
            const resultState = new Map<number, LeagueUiState>(state);

            for (const key of Array.from(resultState.keys())) {
                const item = new LeagueUiState(resultState.get(key));
                item.expanded = !item.expanded;
                resultState.set(key, item);
            }

            return resultState;
        }
        default:
            return state;
    }
}

function toggleSelectionOfLeague(state: Map<number, LeagueUiState>,
                                 leagueId: number,
                                 selected?: boolean): Map<number, LeagueUiState> {
    const resultState = new Map<number, LeagueUiState>(state);

    const previousItem = resultState.get(leagueId);
    const item = new LeagueUiState(previousItem);
    item.selected = selected !== undefined ? Boolean(selected) : !item.selected;
    resultState.set(leagueId, item);

    return resultState;
}