import CommonAction from '../../../../actions/common/types/common-action';
import { UiActionType } from '../../../../actions/ui/ui-action-type';

export default function busyReducer(state: boolean = false, action: CommonAction) {
    if (action.type === UiActionType.SET_BUSY) {
        return action.payload as boolean;
    }
    return state;
}