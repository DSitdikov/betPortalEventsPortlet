import { combineReducers } from 'redux';
import uiCommonReducer from './ui-common/ui-common.reducer';
import leaguesReducer from './leagues/leagues.reducer';
import eventsReducer from './events/events.reducer';
import sportReducer from './sport/sport.reducer';
import marketsReducer from './markets/markets.reducer';
import periodsReducer from './periods/periods.reducer';

const uiReducer = combineReducers({
    common: uiCommonReducer,
    sport: sportReducer,
    leagues: leaguesReducer,
    events: eventsReducer,
    periods: periodsReducer,
    markets: marketsReducer
});

export default uiReducer;