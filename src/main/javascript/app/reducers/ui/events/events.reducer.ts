import CommonAction from '../../../actions/common/types/common-action';
import EventUiState from '../../../entities/ui/event-ui-state/event-ui-state';
import { UiActionType } from '../../../actions/ui/ui-action-type';
import LeagueEntities from '../../../services/data-loaders/league-data-loader/league-entities';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';

const initialState = new Map<number, EventUiState>();

export default function eventsReducer(state: Map<number, EventUiState> = initialState, action: CommonAction) {
    switch (action.type) {
        case DomainModelActionType.FETCH_LEAGUES_SUCCESS: {
            const leagueEntities = action.payload as LeagueEntities;
            const events = leagueEntities.events;
            const resultState = new Map<number, EventUiState>(state);

            for (const eventId of Array.from(events.keys())) {
                if (!resultState.has(eventId)) {
                    resultState.set(eventId, new EventUiState());
                }
            }

            return resultState;
        }
        case UiActionType.TOGGLE_SELECTION_OF_LEAGUE: {
            return toggleSelectionOfEvents(state, action.payload.eventIds);
        }
        case UiActionType.TOGGLE_SELECTION_OF_EVENT: {
            return toggleSelectionOfEvents(state, [action.payload.eventId]);
        }
        default:
            return state;
    }
}

function toggleSelectionOfEvents(state: Map<number, EventUiState>, eventIds: number[]): Map<number, EventUiState> {
    const resultState = new Map<number, EventUiState>(state);

    eventIds.forEach(eventId => {
        const item = new EventUiState(resultState.get(eventId));
        item.selected = !item.selected;
        resultState.set(eventId, item);
    });

    return resultState;
}