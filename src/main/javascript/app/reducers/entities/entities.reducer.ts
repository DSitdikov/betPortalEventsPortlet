import { combineReducers } from 'redux';
import sportReducer from './sport/sport.reducer';
import leaguesReducer from './leagues/leagues.reducer';
import eventsReducer from './events/events.reducer';
import periodsReducer from './periods/periods.reducer';
import marketsReducer from './markets/markets.reducer';

const entitiesReducer = combineReducers({
    sport: sportReducer,
    leagues: leaguesReducer,
    events: eventsReducer,
    periods: periodsReducer,
    markets: marketsReducer
});

export default entitiesReducer;