import CommonAction from '../../../actions/common/types/common-action';
import League from '../../../entities/domain-model/league/league';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';
import LeagueEntities from '../../../services/data-loaders/league-data-loader/league-entities';

const initialState = new Map<number, League>();

export default function leaguesReducer(state: Map<number, League> = initialState, action: CommonAction) {
    if (action.type === DomainModelActionType.FETCH_LEAGUES_SUCCESS) {
        const leagueEntities = action.payload as LeagueEntities;
        return leagueEntities.leagues;
    }
    return state;
}