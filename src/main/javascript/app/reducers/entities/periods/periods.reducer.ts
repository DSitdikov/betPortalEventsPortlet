import CommonAction from '../../../actions/common/types/common-action';
import LeagueEventPeriod from '../../../entities/domain-model/league-event-period/league-event-period';
import LeagueEntities from '../../../services/data-loaders/league-data-loader/league-entities';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';

const initialState = new Map<number, LeagueEventPeriod>();

export default function periodsReducer(state: Map<number, LeagueEventPeriod> = initialState, action: CommonAction) {
    switch (action.type) {
        case DomainModelActionType.FETCH_LEAGUES_SUCCESS: {
            const leagueEntities = action.payload as LeagueEntities;
            return leagueEntities.periods;
        }
        default:
            return state;
    }
}