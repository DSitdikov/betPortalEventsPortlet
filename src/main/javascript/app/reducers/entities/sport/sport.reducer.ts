import Sport from '../../../entities/domain-model/sport/sport';
import env from '../../../env/env';

const initialState = new Sport({ id: env.sportId, name: 'Soccer' });

export default function sportReducer(state: Sport = initialState) {
    return state;
}