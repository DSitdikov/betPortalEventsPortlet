import League from './league';

describe('League', () => {
    it('should create default instance if there are not constructor parameters', () => {
        const league = new League();

        expect(league.id).toEqual(0);
        expect(league.name).toEqual('');
        expect(league.events).toEqual([]);
    });

    it('should parse league', () => {
        const league = new League({
            id: 994,
            name: 'England - Premier League',
            events: [
                {
                    id: 1188,
                    home: 'Huddersfield Town',
                    away: 'Chelsea'
                }
            ]
        });

        expect(league.id).toEqual(994);
        expect(league.name).toEqual('England - Premier League');
        expect(league.events.length).toEqual(1);
        expect(league.events[0]).toEqual(1188);
    });
});