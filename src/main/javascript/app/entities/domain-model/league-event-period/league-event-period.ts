import { TotalType } from '../markets/total-market/total-type';
import TotalMarket from '../markets/total-market/total-market';

export default class LeagueEventPeriod {
    id: number;
    eventId: number;
    number: number;
    moneyLine: number;
    doubleChance: number;
    bts: number;
    handicap: number[];
    totals: number[];
    teamHomeTotals: number[];
    teamAwayTotals: number[];
    allTotals: number[][];

    constructor(data: any = {}) {
        data = data || {};

        this.id = data.id || 0;
        this.eventId = data.eventId || 0;
        this.number = data.number || 0;

        // We have only one moneyline market
        const moneyLines = data.moneylines || [];
        this.moneyLine = (moneyLines[0] || {}).id || 0;

        this.doubleChance = 0;
        this.bts = 0;

        const handicap = data.spreads || [];
        // Order by handicap.hdp ASC
        handicap.sort((a, b) => a.hdp - b.hdp);
        this.handicap = handicap.map(item => item.id);

        const totals = data.totals || [];
        // Order by total.points ASC
        totals.sort((a, b) => a.points - b.points);
        this.totals = totals.map(item => item.id);

        this.teamHomeTotals = (data.teamTotals || []).map(teamTotal => teamTotal.id);
        this.teamAwayTotals = (data.teamTotals || []).map(teamTotal => teamTotal.id * TotalMarket.TOTAL_AWAY_ID_FACTOR);

        this.allTotals = [];
        this.allTotals[TotalType.OVERALL] = this.totals;
        this.allTotals[TotalType.HOME] = this.teamHomeTotals;
        this.allTotals[TotalType.AWAY] = this.teamAwayTotals;
    }
}