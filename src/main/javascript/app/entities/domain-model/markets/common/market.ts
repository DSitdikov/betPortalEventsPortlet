import Entity from '../../entity/entity';
import Price from '../../price/price';
import LeagueEvent from '../../league-event/league-event';
import { MarketType } from './market-type';
import ArrayUtils from '../../../../utils/array-utils';

export default abstract class Market implements Entity {
    protected outcomeTypes: string[];

    id: number;
    periodId: number;
    // It is needed due to team totals have only one id for two totals:
    // outcomeA and outcomeB, so we generate our id that keep in field 'id'
    originId: number;
    type: string;
    outcomeA: Price;
    outcomeB: Price;
    
    private INCORRECT_OUTCOME_NAME = '';

    constructor(data?: any) {
        this.id = data.id || 0;
        this.originId = data.originId || Math.abs(this.id);
        this.periodId = data.periodId || 0;
        this.type = data.type || MarketType.UNKNOWN;
        this.outcomeB = new Price(data.away);
        this.outcomeA = new Price(data.home);
    }

    getOutcomeName(outcomeType: string, event?: LeagueEvent): string {
        if (!ArrayUtils.includes(this.outcomeTypes, outcomeType)) {
            return this.INCORRECT_OUTCOME_NAME;
        }

        return this.selectOutcomeName(outcomeType, event);
    }

    protected abstract selectOutcomeName(outcomeType: string, event?: LeagueEvent);
}
