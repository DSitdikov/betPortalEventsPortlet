export const OutcomeType = {
    HOME: 'home',
    DRAW: 'draw',
    AWAY: 'away',
    OVER: 'over',
    UNDER: 'under',
    NONE: 'none'
};