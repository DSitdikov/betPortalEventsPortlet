import { MarketConstants } from '../../../constants/market.constants';

/**
 * Class describes factor of the bet
 * which client can make on specific outcome (specific team in event
 * and specific market (for example, moneline, double chance etc)).
 *
 * Price is also called 'factor', 'line'. If we say 'no line', we want to say
 * that there is no price (factor) for specific outcome.
 *
 * ATTENTION!!!
 * We do not use calculated fields (for instance, hasValue, formmatted)
 * due to nature of procedure of clone object (ObjectUtils#cloneObject).
 * It does not create real copy, does only copy of real fields.
 *
 * @see ObjectUtils#cloneObject
 */
export default class Price {
    tip: string;
    value: number;
    formatted: string;
    hasValue: boolean;

    constructor(data?: any) {
        data = data || {};

        if (typeof data === 'number') {
            data = {
                value: data
            };
        }

        const decimalSize = MarketConstants.SIZE_OF_DECIMAL_PART;
        const noLine = MarketConstants.NO_LINE;

        this.tip = data.tip || '';
        this.value = data.value || 0;
        this.hasValue = Boolean(this.value);
        this.formatted = this.hasValue ? this.value.toFixed(decimalSize) : noLine;
    }
}