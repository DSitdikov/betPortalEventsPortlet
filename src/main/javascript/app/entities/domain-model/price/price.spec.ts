import Price from './price';

describe('Price', () => {
    it('should create default instance if there are no parameters', () => {
        const price = new Price();

        expect(price.value).toEqual(0);
        expect(price.tip).toEqual('');
        expect(price.formatted).toEqual('No line');
        expect(price.hasValue).toEqual(false);
    });

    it('should correctly process primitive type in constructor', () => {
        const price = new Price(1.52);

        expect(price.value).toEqual(1.52);
        expect(price.tip).toEqual('');
        expect(price.formatted).toEqual('1.52');
        expect(price.hasValue).toEqual(true);
    });

    it('should parse value as number if it is greater than 0', () => {
        const price = new Price({
            value: 1.52
        });

        expect(price.value).toEqual(1.52);
        expect(price.formatted).toEqual('1.52');
        expect(price.hasValue).toEqual(true);
    });

    it('should format number as two decimal places if value has less than two decimal digits', () => {
        const price = new Price(1.5);

        expect(price.value).toEqual(1.5);
        expect(price.formatted).toEqual('1.50');
    });

    it('should format number as two decimal places if value has more than two decimal digits', () => {
        const price = new Price(1.574);

        expect(price.value).toEqual(1.574);
        expect(price.formatted).toEqual('1.57');
    });

    it('should use special format for price without value', () => {
        const price = new Price(0);

        expect(price.formatted).toEqual('No line');
    });

    it('should round value when format it', () => {
        const price = new Price(1.576);

        expect(price.value).toEqual(1.576);
        expect(price.formatted).toEqual('1.58');
    });

    it('should set flag that value exists if it is greater than 0', () => {
        const price = new Price({
            value: 1.5
        });

        expect(price.hasValue).toEqual(true);
    });

    it('should clear flag that value exists if it equals 0', () => {
        const price = new Price(0);

        expect(price.hasValue).toEqual(false);
    });

    it('should not automatically update formatted field and value flag', () => {
        const price = new Price(0);

        expect(price.hasValue).toEqual(false);
        expect(price.formatted).toEqual('No line');

        price.value = 1.25;

        expect(price.hasValue).toEqual(false);
        expect(price.formatted).toEqual('No line');
    });

    it('should set price tip', () => {
        const price = new Price({
            tip: '+0.75'
        });

        expect(price.tip).toEqual('+0.75');
    });
});