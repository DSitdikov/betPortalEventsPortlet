import LeagueEvent from './league-event';

describe('LeagueEvent', () => {
    it('should create default instance if there are no constructor parameters', () => {
        const event = new LeagueEvent();

        expect(event.id).toEqual(0);
        expect(event.name).toEqual('');
        expect(event.away).toEqual('');
        expect(event.home).toEqual('');
        expect(event.startDate).not.toBeNull();
        expect(event.startDateOrigin).not.toBeNull();
        expect(event.startIn).not.toBeNull();
        expect(event.hasValidDate).toEqual(false);
    });

    it('should parse basic properties of an event', () => {
        const event = new LeagueEvent({
            id: 1188,
            startDate: 1513108800000,
            home: 'Huddersfield Town',
            away: 'Chelsea',
            periods: []
        });

        expect(event.id).toEqual(1188);
        expect(event.name).toEqual('Huddersfield Town - Chelsea');
        expect(event.startDateOrigin).not.toBeNull();
        expect(event.startDateOrigin.getTime()).toEqual(1513108800000);
        expect(event.startDate).not.toBeNull();
        expect(event.startDate.getTime()).toEqual(1513108800000 + event.startDate.getTimezoneOffset() * 60 * 1000);
        expect(event.home).toEqual('Huddersfield Town');
        expect(event.away).toEqual('Chelsea');
        expect(event.periods).toEqual([]);
        expect(event.fullTimePeriod).toEqual(0);
    });

    it('should correct copy event', () => {
        const event = new LeagueEvent({
            id: 1188,
            startDate: 1513108800000,
            home: 'Huddersfield Town',
            away: 'Chelsea',
            periods: []
        });
        const eventCopy = new LeagueEvent(event);

        expect(eventCopy.id).toEqual(1188);
        expect(event.name).toEqual('Huddersfield Town - Chelsea');
        expect(eventCopy.startDateOrigin).not.toBeNull();
        expect(eventCopy.startDateOrigin.getTime()).toEqual(1513108800000);
        expect(eventCopy.startDate).not.toBeNull();
        expect(eventCopy.startDate.getTime())
            .toEqual(1513108800000 + eventCopy.startDate.getTimezoneOffset() * 60 * 1000);
        expect(eventCopy.home).toEqual('Huddersfield Town');
        expect(eventCopy.away).toEqual('Chelsea');
        expect(eventCopy.periods).toEqual([]);
        expect(eventCopy.fullTimePeriod).toEqual(0);
    });

    it('should correct get full game period', () => {
        const event = new LeagueEvent({
            id: 1188,
            startDate: 1513108800000,
            home: 'Huddersfield Town',
            away: 'Chelsea',
            periods: [
                {
                    id: 47362,
                    number: 1
                },
                {
                    id: 47365,
                    number: 0
                }
            ]
        });

        expect(event.fullTimePeriod).toEqual(47365);
    });

    it('should build event name on base team names', () => {
        const event = new LeagueEvent({
            home: 'Huddersfield Town',
            away: 'Chelsea'
        });

        expect(event.name).toEqual('Huddersfield Town - Chelsea');
    });
});