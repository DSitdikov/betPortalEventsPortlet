import UiState from './store-state/ui-state';
import DomainModelState from './store-state/domain-model-state';

export default interface StoreState {
    ui: UiState;
    entities: DomainModelState;
}
