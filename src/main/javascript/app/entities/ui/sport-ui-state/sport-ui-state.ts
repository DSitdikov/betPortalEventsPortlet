export default class SportUiState {
    expanded: boolean;

    constructor(data: any = {}) {
        this.expanded = data.expanded !== undefined ? Boolean(data.expanded) : true;
    }
}