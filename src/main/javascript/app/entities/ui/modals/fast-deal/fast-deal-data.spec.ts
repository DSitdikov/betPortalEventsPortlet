import FastDealData from './fast-deal-data';
import Price from '../../../domain-model/price/price';

describe('FastDealData', () => {
    it('should create default instance if there are no parameters', () => {
        const fastDealData = new FastDealData();

        expect(fastDealData.event).toEqual(0);
        expect(fastDealData.price instanceof Price).toBeTruthy();
        expect(fastDealData.marketId).toEqual(0);
        expect(fastDealData.price.hasValue).toBeFalsy();
        expect(fastDealData.betValue).toEqual(5000);
        expect(fastDealData.outcomeType).toEqual('');
        expect(fastDealData.outcomeName).toEqual('');
    });

    it('should parse raw data', () => {
        const fastDealData = new FastDealData({
            event: 101,
            price: new Price(1.27),
            betValue: 7000,
            outcomeType: 'away',
            outcomeName: 'Zenit +0.25',
            marketId: 2446
        });

        expect(fastDealData.marketId).toEqual(2446);
        expect(fastDealData.price.value).toEqual(1.27);
        expect(fastDealData.event).toEqual(101);
        expect(fastDealData.betValue).toEqual(7000);
        expect(fastDealData.outcomeType).toEqual('away');
        expect(fastDealData.outcomeName).toEqual('Zenit +0.25');
    });

    it('should clone object correctly', () => {
        const fastDealData = new FastDealData({
            event: 101,
            price: new Price(1.27),
            betValue: 7000,
            outcomeType: 'away',
            outcomeName: 'Zenit +0.25',
            marketId: 2446
        });
        const fastDealDataCopy = new FastDealData(fastDealData);

        expect(fastDealDataCopy.price instanceof Price).toBeTruthy();
        expect(fastDealDataCopy.marketId).toEqual(2446);
        expect(fastDealDataCopy.price.value).toEqual(1.27);
        expect(fastDealDataCopy.event).toEqual(101);
        expect(fastDealDataCopy.betValue).toEqual(7000);
        expect(fastDealDataCopy.outcomeType).toEqual('away');
        expect(fastDealDataCopy.outcomeName).toEqual('Zenit +0.25');
    });
});
