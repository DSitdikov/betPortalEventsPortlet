import Price from '../../../domain-model/price/price';

export default class FastDealData {
    event: number;
    marketId: number;
    betValue: number;
    price: Price;
    outcomeName: string;
    outcomeType: string;

    constructor(data?: any) {
        data = data || {};

        this.event = data.event || 0;
        this.marketId = data.marketId || 0;
        this.betValue = data.betValue || 5000;
        this.price = new Price(data.price);
        this.outcomeName = data.outcomeName || '';
        this.outcomeType = data.outcomeType || '';
    }
}
