export default class EventUiState {
    selected: boolean;

    constructor(data: any = {}) {
        this.selected = data.selected !== undefined ? Boolean(data.selected) : true;
    }
}