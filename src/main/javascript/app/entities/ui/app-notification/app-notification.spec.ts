import AppNotification from './app-notification';

describe('AppNotification', () => {
    it('should create default instance if there are no constructor parameters', () => {
        const notification = new AppNotification();

        expect(notification.type).toEqual('info');
        expect(notification.title).toEqual('');
        expect(notification.message).toEqual('');
    });

    it('should parse raw object', () => {
        const notification = new AppNotification({
            type: 'danger',
            title: 'Javascript error',
            message: 'Error you guys!'
        });

        expect(notification.type).toEqual('danger');
        expect(notification.title).toEqual('Javascript error');
        expect(notification.message).toEqual('Error you guys!');
    });

    it('should clone notification correctly', () => {
        const notification = new AppNotification({
            type: 'danger',
            title: 'Javascript error',
            message: 'Error you guys!'
        });
        const notificationClone = new AppNotification(notification);

        expect(notificationClone.type).toEqual('danger');
        expect(notification.title).toEqual('Javascript error');
        expect(notificationClone.message).toEqual('Error you guys!');
    });
});