import { AppNotificationType } from './app-notification-type';

export default class AppNotification {
    type: string;
    title: string;
    message: string;

    constructor(data: any = {}) {
        this.type = data.type || AppNotificationType.INFO;
        this.title = data.title || '';
        this.message = data.message || '';
    }
}