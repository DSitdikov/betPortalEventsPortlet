export default class LeagueUiState {
    selected: boolean;
    expanded: boolean;

    constructor(data: any = {}) {
        this.selected = data.selected !== undefined ? Boolean(data.selected) : true;
        this.expanded = data.expanded !== undefined ? Boolean(data.expanded) : true;
    }
}