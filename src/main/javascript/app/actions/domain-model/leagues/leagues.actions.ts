import { ActionCreator } from 'redux';
import { DomainModelActionType } from '../domain-model-action-type';
import LeagueEntities from '../../../services/data-loaders/league-data-loader/league-entities';
import CommonAction from '../../common/types/common-action';

export const fetchLeaguesSuccess: ActionCreator<CommonAction> = (leagueEntities: LeagueEntities) => ({
    type: DomainModelActionType.FETCH_LEAGUES_SUCCESS,
    payload: leagueEntities
});
