import { Action } from 'redux';

export default interface CommonAction extends Action {
    type: string;
    payload: any;
}
