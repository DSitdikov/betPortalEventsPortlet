import { ThunkAction } from 'redux-thunk';
import { Action, ActionCreator, Dispatch } from 'redux';
import { fetchLeaguesSuccess } from '../../';
import StoreState from '../../../entities/store-state';
import { doNothing } from '../do-nothing/do-nothing.action';
import LeagueDataLoader from '../../../services/data-loaders/league-data-loader/league-data-loader';

export const reloadData: ActionCreator<ThunkAction<Promise<Action>, StoreState, void>> = () => {
    return (dispatch: Dispatch<StoreState>, getState: () => StoreState): Promise<Action> => {
        const dataLoader = new LeagueDataLoader();
        const sportId = getState().entities.sport.id;
        return dataLoader.getLeagueEntities(sportId)
            .then(leagueEntities => {
                dispatch(fetchLeaguesSuccess(leagueEntities));
            })
            .catch(() => dispatch(doNothing()))
            .then(() => dispatch(doNothing()));
    };
};