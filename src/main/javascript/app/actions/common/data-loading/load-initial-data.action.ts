import { ThunkAction } from 'redux-thunk';
import { Action, ActionCreator, Dispatch } from 'redux';
import StoreState from '../../../entities/store-state';
import { fetchLeaguesSuccess } from '../../';
import { setBusy, showError } from '../../ui/common/common.actions';
import LeagueDataLoader from '../../../services/data-loaders/league-data-loader/league-data-loader';

export const loadInitialData: ActionCreator<ThunkAction<Promise<Action>, StoreState, void>> = () => {
    return (dispatch: Dispatch<StoreState>, getState: () => StoreState): Promise<Action> => {
        const sportId = getState().entities.sport.id;
        const dataLoader = new LeagueDataLoader();
        console.log('init data');
        dispatch(setBusy(true));
        return dataLoader.getLeagueEntities(sportId)
            .then(leagueEntities => {
                console.log('league entities:', leagueEntities);
                dispatch(fetchLeaguesSuccess(leagueEntities));
            })
            .catch(error => dispatch(showError(error)))
            .then(() => dispatch(setBusy(false)));
    };
};