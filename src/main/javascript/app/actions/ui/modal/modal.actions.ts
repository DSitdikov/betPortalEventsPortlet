import { Action, ActionCreator } from 'redux';
import { UiActionType } from '../ui-action-type';
import { ModalType } from '../../../enums/modal-type';

export const showModal: ActionCreator<Action> = (modalType: ModalType) => ({
    type: UiActionType.SHOW_MODAL,
    payload: modalType
});

export const closeModal: ActionCreator<Action> = () => ({ type: UiActionType.CLOSE_MODAL });