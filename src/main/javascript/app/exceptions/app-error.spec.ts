import AppError from './app-error';

describe('AppError', () => {
    it('should create default instance if there are no constructor parameters', () => {
        const error = new AppError();

        expect(error.code).toEqual(0);
        expect(error.message).toEqual('');
    });

    it('should create error object with code = 0 if string is passed to constructor', () => {
        const error = new AppError('Error message');

        expect(error.code).toEqual(0);
        expect(error.message).toEqual('Error message');
    });

    it('should parse raw error object', () => {
        const error = new AppError({
            code: 101403,
            message: 'Error message'
        });

        expect(error.code).toEqual(101403);
        expect(error.message).toEqual('Error message');
    });
});