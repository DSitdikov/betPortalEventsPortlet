export default class ObjectUtils {

    /**
     * Be careful that you do not get real clone!
     * You only get copy of fields but you lose any information
     * about class of the object!
     *
     * @param obj Any object.
     * @returns {any}
     */
    static cloneObject(obj: any) {
        return JSON.parse(JSON.stringify(obj));
    }
}