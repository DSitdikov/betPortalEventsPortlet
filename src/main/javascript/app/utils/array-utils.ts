export default class ArrayUtils {
    static includes(coll: any, query: any) {
        return Boolean(coll && coll.indexOf(query) !== -1);
    }
}