export const DropdownType = {
    HANDICAP_HOME: 'handicapHome',
    HANDICAP_AWAY: 'handicapAway',
    TOTALS_OVER: 'totalsOver',
    TOTALS_UNDER: 'totalsUnder'
};