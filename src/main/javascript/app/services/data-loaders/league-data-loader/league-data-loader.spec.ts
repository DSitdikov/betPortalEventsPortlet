import LeagueDataLoader from './league-data-loader';
import DataLoader from '../common/data-loader';
import env from '../../../env/env';
import Mock = jest.Mock;

describe('LeagueDataLoader', () => {
    const basicLoader = {
        get: jest.fn(),
        post: jest.fn()
    } as DataLoader;

    let service;

    const SPORT_ID = env.sportId;
    const LEAGUE_ID = 994;

    beforeEach(() => {
        (<Mock> basicLoader.get).mockReturnValue(Promise.resolve([
            {
                id: LEAGUE_ID,
                name: 'England - Premier League',
                events: [
                    {
                        id: 1188,
                        startDate: 1513108800000,
                        home: 'Huddersfield Town',
                        away: 'Chelsea',
                        periods: [
                            {
                                id: 47362,
                                number: 1,
                                cutoff: 1513108800000,
                                spreads: [
                                    {
                                        id: 47363,
                                        away: 1.88,
                                        home: 2.01,
                                        hdp: 0.50
                                    }
                                ],
                                moneylines: [],
                                totals: [
                                    {
                                        id: 47364,
                                        points: 1.00,
                                        over: 1.85,
                                        under: 2.04
                                    }
                                ],
                                teamTotals: []
                            },
                            {
                                id: 47365,
                                number: 0,
                                cutoff: 1513108800000,
                                spreads: [
                                    {
                                        id: 47368,
                                        away: 2.06,
                                        home: 1.84,
                                        hdp: 1.50
                                    },
                                    {
                                        id: 47370,
                                        away: 1.40,
                                        home: 3.01,
                                        hdp: 0.75
                                    },
                                    {
                                        id: 47366,
                                        away: 1.81,
                                        home: 2.10,
                                        hdp: 1.25
                                    },
                                    {
                                        id: 47367,
                                        away: 2.39,
                                        home: 1.62,
                                        hdp: 1.75
                                    },
                                    {
                                        id: 47369,
                                        away: 1.53,
                                        home: 2.59,
                                        hdp: 1.00
                                    }
                                ],
                                moneylines: [
                                    {
                                        id: 47376,
                                        away: 1.34,
                                        home: 10.51,
                                        draw: 5.22
                                    }
                                ],
                                totals: [
                                    {
                                        id: 47371,
                                        points: 2.75,
                                        over: 2.11,
                                        under: 1.80
                                    },
                                    {
                                        id: 47375,
                                        points: 3.25,
                                        over: 2.80,
                                        under: 1.45
                                    },
                                    {
                                        id: 47373,
                                        points: 2.50,
                                        over: 1.88,
                                        under: 2.02
                                    },
                                    {
                                        id: 47374,
                                        points: 3.00,
                                        over: 2.48,
                                        under: 1.57
                                    },
                                    {
                                        id: 47372,
                                        points: 2.25,
                                        over: 1.64,
                                        under: 2.35
                                    }
                                ],
                                teamTotals: [
                                    {
                                        id: 47377,
                                        homePoints: 0.50,
                                        homeOver: 2.10,
                                        homeUnder: 1.80,
                                        awayPoints: 2.00,
                                        awayOver: 1.99,
                                        awayUnder: 1.90
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        id: 1198,
                        startDate: 1513195200000,
                        home: 'Liverpool',
                        away: 'West Brom',
                        periods: [
                            {
                                id: 47532,
                                number: 0,
                                cutoff: 1513195200000,
                                spreads: [
                                    {
                                        id: 47537,
                                        away: 2.78,
                                        home: 1.46,
                                        hdp: -1.25
                                    },
                                    {
                                        id: 47535,
                                        away: 1.77,
                                        home: 2.15,
                                        hdp: -2.00
                                    },
                                    {
                                        id: 47536,
                                        away: 2.35,
                                        home: 1.64,
                                        hdp: -1.50
                                    },
                                    {
                                        id: 47533,
                                        away: 2.05,
                                        home: 1.85,
                                        hdp: -1.75
                                    },
                                    {
                                        id: 47534,
                                        away: 1.59,
                                        home: 2.45,
                                        hdp: -2.25
                                    }
                                ],
                                moneylines: [
                                    {
                                        id: 47543,
                                        away: 13.95,
                                        home: 1.23,
                                        draw: 7.03
                                    }
                                ],
                                totals: [
                                    {
                                        id: 47542,
                                        points: 3.50,
                                        over: 2.37,
                                        under: 1.63
                                    },
                                    {
                                        id: 47541,
                                        points: 3.25,
                                        over: 2.12,
                                        under: 1.79
                                    },
                                    {
                                        id: 47540,
                                        points: 2.75,
                                        over: 1.66,
                                        under: 2.31
                                    },
                                    {
                                        id: 47538,
                                        points: 3.00,
                                        over: 1.86,
                                        under: 2.04
                                    },
                                    {
                                        id: 47539,
                                        points: 2.50,
                                        over: 1.53,
                                        under: 2.59
                                    }
                                ],
                                teamTotals: [
                                    {
                                        id: 47544,
                                        homePoints: 2.50,
                                        homeOver: 2.02,
                                        homeUnder: 1.87,
                                        awayPoints: 0.50,
                                        awayOver: 2.05,
                                        awayUnder: 1.83
                                    }
                                ]
                            },
                            {
                                id: 47545,
                                number: 1,
                                cutoff: 1513195200000,
                                spreads: [
                                    {
                                        id: 47546,
                                        away: 2.03,
                                        home: 1.85,
                                        hdp: -0.75
                                    }
                                ],
                                moneylines: [],
                                totals: [
                                    {
                                        id: 47547,
                                        points: 1.25,
                                        over: 1.88,
                                        under: 2.00
                                    }
                                ],
                                teamTotals: []
                            }
                        ]
                    },
                    {
                        id: 1236,
                        startDate: 1512226800000,
                        home: 'Stoke City',
                        away: 'Swansea City',
                        periods: []
                    }
                ]
            }
        ]));

        service = new LeagueDataLoader({ loader: basicLoader });
    });

    describe('#getLeagueEntities', () => {

        it('should return leagues as part of prepared data', () => {
            return service.getLeagueEntities(SPORT_ID)
                .then(entities => {
                    const leagues = entities.leagues;
                    expect(leagues.size).toEqual(1);

                    const league = leagues.get(LEAGUE_ID);
                    expect(league.id).toEqual(LEAGUE_ID);
                    expect(league.events).toEqual([1188, 1198, 1236]);
                });
        });

        it('should return events as part of prepared data', () => {
            return service.getLeagueEntities(SPORT_ID)
                .then(entities => {
                    const events = entities.events;
                    expect(events.size).toEqual(3);

                    let event = events.get(1188);
                    expect(event.id).toEqual(1188);
                    expect(event.leagueId).toEqual(LEAGUE_ID);
                    expect(event.periods).toEqual([47362, 47365]);

                    event = events.get(1198);
                    expect(event.id).toEqual(1198);
                    expect(event.leagueId).toEqual(LEAGUE_ID);
                    expect(event.periods).toEqual([47532, 47545]);

                    event = events.get(1236);
                    expect(event.id).toEqual(1236);
                    expect(event.leagueId).toEqual(LEAGUE_ID);
                    expect(event.periods).toEqual([]);
                });
        });

        it('should return periods as part of prepared data', () => {
            return service.getLeagueEntities(SPORT_ID)
                .then(entities => {
                    const periods = entities.periods;
                    expect(periods.size).toEqual(4);

                    let period = periods.get(47362);
                    expect(period.id).toEqual(47362);
                    expect(period.eventId).toEqual(1188);
                    expect(period.moneyLine).toEqual(0);
                    expect(period.handicap).toEqual([47363]);
                    expect(period.totals).toEqual([47364]);
                    expect(period.teamHomeTotals).toEqual([]);
                    expect(period.teamAwayTotals).toEqual([]);

                    period = periods.get(47365);
                    expect(period.id).toEqual(47365);
                    expect(period.eventId).toEqual(1188);
                    expect(period.moneyLine).toEqual(47376);
                    expect(period.handicap).toEqual([47370, 47369, 47366, 47368, 47367]);
                    expect(period.totals).toEqual([47372, 47373, 47371, 47374, 47375]);
                    expect(period.teamHomeTotals).toEqual([47377]);
                    expect(period.teamAwayTotals).toEqual([-47377]);
                });
        });

        it('should return markets as part of prepared data', () => {
            return service.getLeagueEntities(SPORT_ID)
                .then(entities => {
                    const markets = entities.markets;
                    expect(markets.size).toEqual(30);

                    let market = markets.get(47543);
                    expect(market.id).toEqual(47543);
                    expect(market.originId).toEqual(47543);
                    expect(market.periodId).toEqual(47532);

                    market = markets.get(47544);
                    expect(market.id).toEqual(47544);
                    expect(market.originId).toEqual(47544);
                    expect(market.periodId).toEqual(47532);
                    expect(market.outcomeA.value).toEqual(2.02);

                    market = markets.get(-47544);
                    expect(market.id).toEqual(-47544);
                    expect(market.originId).toEqual(47544);
                    expect(market.periodId).toEqual(47532);
                    expect(market.outcomeA.value).toEqual(2.05);
                });
        });

        it('should pass sport id to data loader', () => {
            service.getLeagueEntities(SPORT_ID);
            expect((<Mock> basicLoader.get).mock.calls[0][0].includes(SPORT_ID)).toBeTruthy();
        });
    });
});