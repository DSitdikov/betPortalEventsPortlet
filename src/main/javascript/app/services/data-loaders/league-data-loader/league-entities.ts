import LeagueEvent from '../../../entities/domain-model/league-event/league-event';
import League from '../../../entities/domain-model/league/league';
import LeagueEventPeriod from '../../../entities/domain-model/league-event-period/league-event-period';
import Market from '../../../entities/domain-model/markets/common/market';

export default interface LeagueEntities {
    leagues: Map<number, League>;
    events: Map<number, LeagueEvent>;
    periods: Map<number, LeagueEventPeriod>;
    markets: Map<number, Market>;
}