import * as React from 'react';
import LeagueEvent from '../../../../entities/domain-model/league-event/league-event';

interface PropsType {
    eventsPublished: LeagueEvent[];
}

type Props = PropsType;

export default class Tabs extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <div className="a-tabs">
                <div className="a-tabs__item a-tabs__item_selected">
                    <div className="a-tabs__item-title">Prematch</div>
                    <div className="a-tabs__item-counter">{this.props.eventsPublished.length}</div>
                </div>
                <div className="a-tabs__item"/>
                <div className="a-tabs__item"/>
            </div>
        );
    }
}
