import * as React from 'react';
import Tabs from './tabs/tabs';
import EventList from './event-list/event-list';
import LeagueDataLoader from "../../../services/data-loaders/league-data-loader/league-data-loader";
import env from "../../../env/env";
import CommonAction from "../../../actions/common/types/common-action";
import {connect, Dispatch} from "react-redux";
import StoreState from "../../../entities/store-state";
import {loadInitialData, reloadData} from "../../../actions";
import EventUiItem from "../../../entities/ui/event-ui-state/event-ui-state";
import LeagueEvent from "../../../entities/domain-model/league-event/league-event";
import {ThunkAction} from "redux-thunk";
import {Action} from "redux";

interface StateToPropsType {
    events: Map<number, LeagueEvent>;
    eventUiState: Map<number, EventUiItem>;
}

interface StateType {
    currentSelectedEvents: string;
}

interface DispathToPropsType {
    init: () => Action;
    reload: (callback: any) => ThunkAction<Promise<Action>, StoreState, void>;
}

type Props = StateToPropsType & DispathToPropsType;

class EventsContent extends React.Component<Props, StateType> {
    updateSelectedEventsTimeoutId: number;
    refreshDataTimeoutId: number;

    constructor(props: Props) {
        super(props);

        this.props.init();
        this.updateSelectedEventsTimeoutId = 0;
        this.refreshDataTimeoutId = 0;
        this.state = {
            currentSelectedEvents: ''
        };
    }

    refreshData() {
        console.log('refresh data');
        if (!env.refreshInterval) {
            return;
        }

        this.refreshDataTimeoutId = window.setTimeout(
            () => this.props.reload(this.refreshData.bind(this)),
            env.refreshInterval
        );
    }

    componentWillMount() {
        this.updateSelectedEvents();
        this.refreshData();
    }

    componentWillUnmount() {
        window.clearTimeout(this.updateSelectedEventsTimeoutId);
        window.clearTimeout(this.refreshDataTimeoutId);
    }

    updateSelectedEvents() {
        if (!env.selectedEventsUpdateInterval) {
            return;
        }

        this.updateSelectedEventsTimeoutId = window.setTimeout(
            () => {
                const selectedEvents = LeagueDataLoader.loadSelectedEvents();

                if (!selectedEvents) {
                    this.setState({
                        currentSelectedEvents: '[]'
                    });
                }

                if (selectedEvents && selectedEvents !== this.state.currentSelectedEvents) {
                    this.setState({
                        currentSelectedEvents: selectedEvents
                    });
                    console.log('events has been updated', this.state.currentSelectedEvents);
                }

                this.updateSelectedEvents();
            },
            env.selectedEventsUpdateInterval
        )
    }

    get eventsPublished(): LeagueEvent[] {
        const events = Array.from(this.props.events.values());
        return events.filter(event => this.state.currentSelectedEvents.includes(event.id.toString()));
    }

    render() {
        return (
            <div className="a-content">
                <Tabs eventsPublished={this.eventsPublished}/>
                <EventList eventsPublished={this.eventsPublished}/>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        events: state.entities.events,
        eventUiState: state.ui.events,
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        init: () => dispatch<any>(loadInitialData()),
        reload: (callback: any) => dispatch<any>(reloadData()).then(() => callback())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(EventsContent);
