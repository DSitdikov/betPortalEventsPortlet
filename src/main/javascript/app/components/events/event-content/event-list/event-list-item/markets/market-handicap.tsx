import * as React from 'react';
import FastDealData from '../../../../../../entities/ui/modals/fast-deal/fast-deal-data';
import { Action } from 'redux';
import LeagueEvent from '../../../../../../entities/domain-model/league-event/league-event';
import { DropdownTwinSelect } from '../dropdown-twin-select/dropdown-twin-select';
import { DropdownType } from '../../../../../../enums/dropdown-type';
import DropdownItem from '../../../../../../entities/ui/dropdown-item/dropdown-item';
import HandicapMarket from '../../../../../../entities/domain-model/markets/handicap-market/handicap-market';
import { OutcomeType } from '../../../../../../entities/domain-model/outcome/outcome-type';
import { connect, Dispatch } from 'react-redux';
import StoreState from '../../../../../../entities/store-state';
import Market from '../../../../../../entities/domain-model/markets/common/market';
import LeagueEventPeriod from '../../../../../../entities/domain-model/league-event-period/league-event-period';
import CommonAction from '../../../../../../actions/common/types/common-action';
import { selectMarket, selectLine } from '../../../../../../actions';
import MarketUiState from '../../../../../../entities/ui/market-ui-state/market-ui-state';
import PeriodUiState from '../../../../../../entities/ui/period-ui-state/period-ui-state';

interface StateToPropsType {
    periods: Map<number, LeagueEventPeriod>;
    markets: Map<number, Market>;
    marketUiState: Map<number, MarketUiState>;
    periodUiState: Map<number, PeriodUiState>;
}

interface DispathToPropsType {
    onSelectLine: (data: FastDealData) => Action;
    onSelectHandicap: (id: number) => Action;
}

interface Props extends StateToPropsType, DispathToPropsType {
    event: LeagueEvent;
}

class MarketHandicap extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    isItemSelected(item: HandicapMarket): boolean {
        return (this.props.marketUiState.get(item.id) || {} as MarketUiState).selected;
    }

    onFastDealHandicap(isHome: boolean) {
        const event = this.props.event;
        const fullTimePeriod = this.props.periods.get(event.fullTimePeriod) as LeagueEventPeriod;
        const handicap = fullTimePeriod.handicap.map(marketId => this.props.markets.get(marketId) as HandicapMarket);
        const selectedHandicap = handicap.find(item => this.isItemSelected(item));

        if (!selectedHandicap) {
            return;
        }

        const price = isHome ? selectedHandicap.outcomeA : selectedHandicap.outcomeB;
        this.props.onSelectLine(new FastDealData({
            marketId: selectedHandicap.originId,
            price: price,
            event: event.id,
            outcomeName: isHome ? `${event.home} ${price.tip}` : `${event.away} ${price.tip}`,
            outcomeType: isHome ? OutcomeType.HOME : OutcomeType.AWAY
        }));
    }

    render() {
        const event = this.props.event;
        const period = this.props.periods.get(event.fullTimePeriod) || new LeagueEventPeriod();
        const handicap = period.handicap.map(marketId => this.props.markets.get(marketId) as HandicapMarket);

        const handicapHomeDropdownItems = handicap.map(item => {
            return new DropdownItem({
                id: item.id,
                title: item.outcomeA.tip,
                value: item.outcomeA.formatted,
                selected: this.isItemSelected(item)
            });
        });

        const handicapAwayDropdownItems = handicap.map(item => {
            return new DropdownItem({
                id: item.id,
                title: item.outcomeB.tip,
                value: item.outcomeB.formatted,
                selected: this.isItemSelected(item)
            });
        });

        return (
            <div className="a-event-market a-event-market_type_handicap">
                <div className="a-event-market__title">Handicap</div>
                <div className="a-event-market__line">
                    <DropdownTwinSelect
                        type={DropdownType.HANDICAP_HOME}
                        items={handicapHomeDropdownItems}
                        onSelectItem={this.props.onSelectHandicap}
                        onClickValue={() => this.onFastDealHandicap(true)}
                    />
                </div>
                <div className="a-event-market__line"/>
                <div className="a-event-market__line">
                    <DropdownTwinSelect
                        type={DropdownType.HANDICAP_AWAY}
                        items={handicapAwayDropdownItems}
                        onSelectItem={this.props.onSelectHandicap}
                        onClickValue={() => this.onFastDealHandicap(false)}
                    />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        periods: state.entities.periods,
        markets: state.entities.markets,
        marketUiState: state.ui.markets,
        periodUiState: state.ui.periods
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onSelectHandicap: (id: number) => dispatch<any>(selectMarket(id)),
        onSelectLine: (data: FastDealData) => dispatch<any>(selectLine(data))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MarketHandicap);
