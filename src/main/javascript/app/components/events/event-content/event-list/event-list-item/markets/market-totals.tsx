import * as React from 'react';
import FastDealData from '../../../../../../entities/ui/modals/fast-deal/fast-deal-data';
import { Action } from 'redux';
import LeagueEvent from '../../../../../../entities/domain-model/league-event/league-event';
import { DropdownTwinSelect } from '../dropdown-twin-select/dropdown-twin-select';
import { DropdownType } from '../../../../../../enums/dropdown-type';
import DropdownItem from '../../../../../../entities/ui/dropdown-item/dropdown-item';
import { OutcomeType } from '../../../../../../entities/domain-model/outcome/outcome-type';
import classNames from 'classnames';
import { TotalType } from '../../../../../../entities/domain-model/markets/total-market/total-type';
import { connect, Dispatch } from 'react-redux';
import StoreState from '../../../../../../entities/store-state';
import Market from '../../../../../../entities/domain-model/markets/common/market';
import LeagueEventPeriod from '../../../../../../entities/domain-model/league-event-period/league-event-period';
import CommonAction from '../../../../../../actions/common/types/common-action';
import TotalMarket from '../../../../../../entities/domain-model/markets/total-market/total-market';
import { selectLine, selectMarket, toggleTotalType } from '../../../../../../actions';
import MarketUiState from '../../../../../../entities/ui/market-ui-state/market-ui-state';
import PeriodUiState from '../../../../../../entities/ui/period-ui-state/period-ui-state';

interface StateToPropsType {
    periods: Map<number, LeagueEventPeriod>;
    markets: Map<number, Market>;
    marketUiState: Map<number, MarketUiState>;
    periodUiState: Map<number, PeriodUiState>;
}

interface DispathToPropsType {
    onSelectLine: (data: FastDealData) => Action;
    onToggleTotalType: (periodId: number, totalType: number) => Action;
    onSelectTotal: (id: number) => Action;
}

interface Props extends StateToPropsType, DispathToPropsType {
    event: LeagueEvent;
}

class MarketTotals extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
        this.state = {
            totalType: TotalType.OVERALL
        };
    }

    isItemSelected(item: TotalMarket): boolean {
        return (this.props.marketUiState.get(item.id) || {} as MarketUiState).selected;
    }

    getSelectedTotals(): TotalMarket[] {
        const event = this.props.event;
        const period = this.props.periods.get(event.fullTimePeriod) || new LeagueEventPeriod();
        const periodUiState = this.props.periodUiState.get(period.id) || {} as PeriodUiState;
        const selectedTotalsType = periodUiState.selectedTotalsType || TotalType.OVERALL;
        const selectedTotalIds = period.allTotals[selectedTotalsType] || [];

        return selectedTotalIds.map(marketId => this.props.markets.get(marketId) as TotalMarket);
    }

    onFastDealTotal(isOver: boolean) {
        const event = this.props.event;
        const totals = this.getSelectedTotals();
        const selectedTotal = totals.find(item => this.isItemSelected(item));

        if (!selectedTotal) {
            return;
        }

        const price = isOver ? selectedTotal.outcomeA : selectedTotal.outcomeB;
        this.props.onSelectLine(new FastDealData({
            // We get origin id due to specific behaviour of team totals
            marketId: selectedTotal.originId,
            price: price,
            event: event.id,
            outcomeName: price.tip,
            outcomeType: isOver ? OutcomeType.OVER : OutcomeType.UNDER
        }));
    }

    render() {
        const event = this.props.event;
        const period = this.props.periods.get(event.fullTimePeriod) || new LeagueEventPeriod();
        const periodUiState = this.props.periodUiState.get(period.id) || {} as PeriodUiState;
        const selectedTotalsType = periodUiState.selectedTotalsType || TotalType.OVERALL;
        const totals = this.getSelectedTotals();

        const iT1Classes = classNames({
            'a-event-market__individual-total': true,
            'a-event-market__individual-total_selected': selectedTotalsType === TotalType.HOME
        });

        const iT2Classes = classNames({
            'a-event-market__individual-total': true,
            'a-event-market__individual-total_selected': selectedTotalsType === TotalType.AWAY
        });

        const totalsOverDropdownItems = totals.map(item => {
            return new DropdownItem({
                id: item.id,
                title: item.outcomeA.tip,
                value: item.outcomeA.formatted,
                selected: this.isItemSelected(item)
            });
        });

        const totalsUnderDropdownItems = totals.map(item => {
            return new DropdownItem({
                id: item.id,
                title: item.outcomeB.tip,
                value: item.outcomeB.formatted,
                selected: this.isItemSelected(item)
            });
        });

        return (
            <div className="a-event-market a-event-market_type_totals">
                <div className="a-event-market__title">Totals</div>
                <div className="a-event-market__line">
                    <DropdownTwinSelect
                        type={DropdownType.TOTALS_OVER}
                        items={totalsOverDropdownItems}
                        onSelectItem={this.props.onSelectTotal}
                        onClickValue={() => this.onFastDealTotal(true)}
                    />
                </div>
                <div className="a-event-market__line a-event-market__line_content-pos_left">
                    <div
                        className={iT1Classes}
                        onClick={() => this.props.onToggleTotalType(period.id, TotalType.HOME)}
                    >
                        iT1
                    </div>
                    <div
                        className={iT2Classes}
                        onClick={() => this.props.onToggleTotalType(period.id, TotalType.AWAY)}
                    >
                        iT2
                    </div>
                </div>
                <div className="a-event-market__line">
                    <DropdownTwinSelect
                        type={DropdownType.TOTALS_UNDER}
                        items={totalsUnderDropdownItems}
                        onSelectItem={this.props.onSelectTotal}
                        onClickValue={() => this.onFastDealTotal(false)}
                    />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        periods: state.entities.periods,
        markets: state.entities.markets,
        marketUiState: state.ui.markets,
        periodUiState: state.ui.periods
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onSelectTotal: (id: number) => dispatch<any>(selectMarket(id)),
        onToggleTotalType: (periodId: number, totalType: number) => dispatch(toggleTotalType(periodId, totalType)),
        onSelectLine: (data: FastDealData) => dispatch<any>(selectLine(data))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MarketTotals);
