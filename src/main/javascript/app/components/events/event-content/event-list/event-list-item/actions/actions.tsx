import * as React from 'react';
import LeagueEvent from '../../../../../../entities/domain-model/league-event/league-event';

interface Props {
    event: LeagueEvent;
}

export default class Actions extends React.Component<Props> {

    constructor(props: any) {
        super(props);
    }

    render() {
        const event = this.props.event;

        return (
            <div className="a-event__actions">
                <div className="a-event__actions-title">
                    <span className="a-event__id hidden">#122</span>
                    <span className="a-event__time">
                        <span className="hidden">/</span>
                        <span>{event.startTimeFormatted}</span>
                    </span>
                </div>
                <div className="a-event__actions-line a-event__actions-line_type_time"/>
                <div className="a-event__actions-line a-event__actions-line_type_time"/>
                <div className="a-event__actions-line a-event__actions-line_type_time">
                    <div className="a-event__time-value a-event__time-value_active">FT</div>
                </div>
            </div>
        );
    }
}
