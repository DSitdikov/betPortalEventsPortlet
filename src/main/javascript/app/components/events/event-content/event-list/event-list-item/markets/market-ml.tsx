import * as React from 'react';
import FastDealData from '../../../../../../entities/ui/modals/fast-deal/fast-deal-data';
import { OutcomeType } from '../../../../../../entities/domain-model/outcome/outcome-type';
import { Action } from 'redux';
import LeagueEvent from '../../../../../../entities/domain-model/league-event/league-event';
import { connect, Dispatch } from 'react-redux';
import LeagueEventPeriod from '../../../../../../entities/domain-model/league-event-period/league-event-period';
import CommonAction from '../../../../../../actions/common/types/common-action';
import StoreState from '../../../../../../entities/store-state';
import SimpleLineMarket from '../../../../../../entities/domain-model/markets/simple-line-market/simple-line-market';
import Market from '../../../../../../entities/domain-model/markets/common/market';
import { selectLine } from '../../../../../../actions';
import MoneyLineMarket from '../../../../../../entities/domain-model/markets/money-line-market/money-line-market';

interface StateToPropsType {
    periods: Map<number, LeagueEventPeriod>;
    markets: Map<number, Market>;
}

interface DispathToPropsType {
    onSelectLine: (data: FastDealData) => Action;
}

interface Props extends DispathToPropsType, StateToPropsType {
    event: LeagueEvent;
}

class MarketMl extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        const outcomeNoLineClass = 'a-event-market__outcome-value a-event-market__outcome-value_type_no-line';
        const outcomeHasValueClass = 'a-event-market__outcome-value';

        const event = this.props.event;
        const fullTimePeriod = this.props.periods.get(event.fullTimePeriod) || new LeagueEventPeriod();
        const markets = this.props.markets;
        const moneyLine = markets.get(fullTimePeriod.moneyLine) as SimpleLineMarket || new MoneyLineMarket();

        return (
            <div className="a-event-market a-event-market_type_ml">
                <div className="a-event-market__title">ML</div>
                <div className="a-event-market__line">
                    <div
                        onClick={() => {
                            this.props.onSelectLine(new FastDealData({
                                marketId: moneyLine.originId,
                                price: moneyLine.outcomeA,
                                event: event.id,
                                outcomeName: `${event.home} ML`,
                                outcomeType: OutcomeType.HOME
                            }));
                        }}
                        className={moneyLine.outcomeA.hasValue ? outcomeHasValueClass : outcomeNoLineClass}
                    >
                        {moneyLine.outcomeA.formatted}
                    </div>
                </div>
                <div className="a-event-market__line">
                    <div
                        onClick={() => {
                            this.props.onSelectLine(new FastDealData({
                                marketId: moneyLine.originId,
                                price: moneyLine.draw,
                                event: event.id,
                                outcomeName: `Draw ML`,
                                outcomeType: OutcomeType.DRAW
                            }));
                        }}
                        className={moneyLine.draw.hasValue ? outcomeHasValueClass : outcomeNoLineClass}
                    >
                        {moneyLine.draw.formatted}
                    </div>
                </div>
                <div className="a-event-market__line">
                    <div
                        onClick={() => {
                            this.props.onSelectLine(new FastDealData({
                                marketId: moneyLine.originId,
                                price: moneyLine.outcomeB,
                                event: event.id,
                                outcomeName: `${event.away} ML`,
                                outcomeType: OutcomeType.AWAY
                            }));
                        }}
                        className={moneyLine.outcomeB.hasValue ? outcomeHasValueClass : outcomeNoLineClass}
                    >
                        {moneyLine.outcomeB.formatted}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        periods: state.entities.periods,
        markets: state.entities.markets
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onSelectLine: (data: FastDealData) => dispatch<any>(selectLine(data))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MarketMl);
