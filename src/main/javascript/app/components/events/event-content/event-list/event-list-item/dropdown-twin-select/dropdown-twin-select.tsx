import * as React from 'react';
import DropdownItem from '../../../../../../entities/ui/dropdown-item/dropdown-item';
import { Action } from 'redux';
import classNames from 'classnames';

interface Props {
    type: string;
    items: DropdownItem[];
    onSelectItem: (id: number) => Action;
    onClickValue: () => void;
}

interface State {
    opened: boolean;
}

export class DropdownTwinSelect extends React.Component<Props, State> {
    elementRef: HTMLElement | null;

    constructor(props: Props) {
        super(props);

        this.state = {
            opened: false
        };
    }

    handleOpenBtnClick() {
        this.toggleOpenDropdown();
    }

    componentDidMount() {
        document.addEventListener('mousedown', (e) => this.handleClickOutside(e));
    }

    componentWillUnmount() {
        // TODO: remove listener if dropdown is closed to avoid unnecessary actions
        document.removeEventListener('mousedown', (e) => this.handleClickOutside(e));
    }

    setElementRef(el: HTMLElement | null) {
        this.elementRef = el;
    }

    closeDropdown() {
        this.setState({
            opened: false
        });
    }

    toggleOpenDropdown() {
        this.setState({
            opened: !this.state.opened
        });
    }

    handleClickOutside(event: Event) {
        if (this.elementRef && !this.elementRef.contains(event.target as HTMLElement)) {
            this.closeDropdown();
        }
    }

    handleSelectItem(item: DropdownItem) {
        this.props.onSelectItem(item.id);
        this.closeDropdown();
    }

    render() {
        const items = this.props.items;
        const current = items.filter(item => item.selected)[0];
        const opened = this.state.opened;

        const dropdownBtnOpenClass = classNames({
            'a-dropdown-twin-select__btn-open': true,
            'a-dropdown-twin-select__btn-open_active': opened
        });

        const dropdownStyleClass = classNames({
            'a-dropdown-twin-select': true,
            'a-dropdown-twin-select_type_no-line': !this.props.items.length
        });

        const dropdownOptionsOpenedStyleClass = classNames({
            'a-dropdown-twin-select__options': true,
            'a-dropdown-twin-select__options_opened': opened
        });

        return (
            <div className={dropdownStyleClass} ref={(el) => this.setElementRef(el)}>
                <div className="a-dropdown-twin-select__current-option" onClick={() => this.props.onClickValue()}>
                    <div className="a-dropdown-twin-select__title">{current ? current.title : 'NO LINE'}</div>
                    <div className="a-dropdown-twin-select__value">{current ? current.value : ''}</div>
                </div>
                <div className={dropdownBtnOpenClass} onClick={() => this.handleOpenBtnClick()}>
                    <div className="a-vip-icon a-vip-icon_arrow-dropdown"/>
                </div>
                <div className={dropdownOptionsOpenedStyleClass}>
                    {items.map(item => (
                        <div
                            key={item.title}
                            className={
                                [
                                    'a-dropdown-twin-select__option',
                                    item.selected ? 'a-dropdown-twin-select__option_selected' : ''
                                ].join(' ')
                            }
                            onClick={() => this.handleSelectItem(item)}
                        >
                            <div className="a-dropdown-twin-select__title">{item.title}</div>
                            <div className="a-dropdown-twin-select__value">{item.value}</div>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}
