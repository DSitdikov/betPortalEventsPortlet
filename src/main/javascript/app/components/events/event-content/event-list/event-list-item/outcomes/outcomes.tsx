import * as React from 'react';
import LeagueEvent from '../../../../../../entities/domain-model/league-event/league-event';

interface Props {
    event: LeagueEvent;
}

export default class Outcomes extends React.Component<Props> {

    constructor(props: any) {
        super(props);
    }

    render() {
        const event = this.props.event;

        return (
            <div className="a-event__outcomes">
                <div className="a-event__outcomes-title">Starts in {event.startIn}</div>
                <div className="a-event__outcomes-item">{event.home}</div>
                <div className="a-event__outcomes-item">Draw</div>
                <div className="a-event__outcomes-item">{event.away}</div>
            </div>
        );
    }
}
