import * as React from 'react';
import LeagueEvent from '../../../../../entities/domain-model/league-event/league-event';
import Actions from './actions/actions';
import Outcomes from './outcomes/outcomes';
import MarketMl from './markets/market-ml';
import MarketDc from './markets/market-dc';
import MarketHandicap from './markets/market-handicap';
import MarketTotals from './markets/market-totals';
import MarketBts from './markets/market-bts';

interface Props {
    event: LeagueEvent;
}

export class EventListItem extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        const event = this.props.event;

        return (
            <div className="a-event">
                <Actions event={event}/>
                <Outcomes event={event}/>
                <MarketMl event={event}/>
                <MarketDc event={event}/>
                <MarketHandicap event={event}/>
                <MarketTotals event={event}/>
                <MarketBts event={event}/>
            </div>
        );
    }
}
