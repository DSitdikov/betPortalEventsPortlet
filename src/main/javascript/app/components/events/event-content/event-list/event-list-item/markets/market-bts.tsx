import * as React from 'react';
import FastDealData from '../../../../../../entities/ui/modals/fast-deal/fast-deal-data';
import { Action } from 'redux';
import LeagueEvent from '../../../../../../entities/domain-model/league-event/league-event';
import { connect, Dispatch } from 'react-redux';
import CommonAction from '../../../../../../actions/common/types/common-action';
import { selectLine } from '../../../../../../actions';
import BothToScoreMarket
    from '../../../../../../entities/domain-model/markets/both-to-score-market/both-to-score-market';

interface DispathToPropsType {
    onSelectLine: (data: FastDealData) => Action;
}

interface Props extends DispathToPropsType {
    event: LeagueEvent;
}

class MarketBts extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        const outcomeNoLineClass = 'a-event-market__outcome-value a-event-market__outcome-value_type_no-line';
        const outcomeHasValueClass = 'a-event-market__outcome-value';
        const bts = new BothToScoreMarket();

        return (
            <div className="a-event-market a-event-market_type_bts">
                <div className="a-event-market__title">BTS</div>
                <div className="a-event-market__line">
                    <div className="a-event-market__outcome-subtitle">YES</div>
                    <div className={bts.outcomeA.hasValue ? outcomeHasValueClass : outcomeNoLineClass}>
                        {bts.outcomeA.formatted}
                    </div>
                </div>
                <div className="a-event-market__line"/>
                <div className="a-event-market__line">
                    <div className="a-event-market__outcome-subtitle">NO</div>
                    <div className={bts.outcomeB.hasValue ? outcomeHasValueClass : outcomeNoLineClass}>
                        {bts.outcomeB.formatted}
                    </div>
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onSelectLine: (data: FastDealData) => dispatch<any>(selectLine(data))
    };
}

export default connect(null, mapDispatchToProps)(MarketBts);
