import * as React from 'react';
import classNames from 'classnames';

interface Props {
    title: string;
    busy: boolean;
    disabled?: boolean;
    handleAction: any;
}

export default class Btn extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    handleClick<P>(e: React.MouseEvent<P>) {
        e.preventDefault();
        this.props.handleAction();
    }

    render() {
        const btnClasses = classNames({
            'a-btn': true,
            'a-btn_mode_disabled': this.props.disabled,
            'a-btn_mode_busy': this.props.busy
        });

        return (
            <button type="submit" onClick={(e) => this.handleClick(e)} className={btnClasses}>
                <span className="a-btn__title">{this.props.title}</span>
                <div className="a-btn__loader"/>
            </button>
        );
    }
}