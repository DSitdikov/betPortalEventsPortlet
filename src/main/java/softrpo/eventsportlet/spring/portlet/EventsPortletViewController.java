package softrpo.eventsportlet.spring.portlet;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceResponse;
import javax.portlet.ResourceURL;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.WebKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import softrpo.eventsportlet.spring.services.PriceProcessorService;

import static com.liferay.portal.kernel.util.PortalUtil.getPortletId;

/**
 * @author softpro
 */
@Controller
@RequestMapping("VIEW")
public class EventsPortletViewController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventsPortletViewController.class);

    @Autowired
    PriceProcessorService priceProcessorService;

    @RenderMapping
    public String view(RenderRequest request, RenderResponse response, ModelMap model) {
        User user = (User) request.getAttribute(WebKeys.USER);
        String userScreenName = user != null ? user.getScreenName() : "anonymous";

        ResourceURL baseResourceUrl = response.createResourceURL();

        model.addAttribute("portletResourceUrl", baseResourceUrl.toString());
        model.addAttribute("standalone", false);
        model.addAttribute("authenticatedUser", userScreenName);
        model.addAttribute("portletId", getPortletId(request));
        model.addAttribute("portletAppContextPath", request.getContextPath());

        return "view";
    }

    @ResourceMapping("events")
    public void events(ResourceResponse response) throws Exception {
        LOGGER.debug("called events method");

        String eventJson = this.priceProcessorService.getEvents();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getPortletOutputStream().write(eventJson.getBytes());
    }
}